//
//  SheepView.swift
//  sheep
//
//  Created by Naoki Takimura on 6/5/14.
//  Copyright (c) 2014 Naoki Takimura. All rights reserved.
//

import Cocoa

class Sheep {
    var x = 0
    var y = 0
    var jumpX = 0
    var stretch = false
}

class SheepView : NSView {

    // MARK: Managing window
    
    private var hideTitle = true {
        didSet {
            if let window = (NSApplication.shared().delegate as? AppDelegate)?.window {
                if hideTitle {
                    window.styleMask = NSBorderlessWindowMask
                } else {
                    window.styleMask = .closable
                }
            }
        }
    }

    override func viewDidMoveToSuperview() {
        Timer.scheduledTimer(timeInterval: 0.1, target: self,
            selector: #selector(SheepView.update), userInfo: nil, repeats: true)
    }
    
    // MARK: Mouse event
    
    private var appendingSheep = false

    override func mouseDown(with event: NSEvent) {
        appendingSheep = true
    }
    
    override func mouseUp(with event: NSEvent) {
        appendingSheep = false
        
        if (event.clickCount == 2) {
            hideTitle = !hideTitle
        }
    }
    
    // MARK: Timer event

    func update() {
        calc()
        setNeedsDisplay(NSRect(x: 0, y: 0, width: 100, height: 100))
    }

    private func calc() {

        if appendingSheep {
            sheepFlock.append(sheep())
        }
        
        var newSheepFlock = [Sheep]()
        for sheep in sheepFlock {
            sheep.x -= 5

            if sheep.jumpX - 20 <= sheep.x && sheep.x < sheep.jumpX {
                sheep.y += 3
                if sheep.jumpX - 20 <= sheep.x && sheep.x < sheep.jumpX - 15  {
                    count += 1
                }
            } else if sheep.jumpX - 40 <= sheep.x && sheep.x < sheep.jumpX - 20 {
                sheep.y -= 3
            }

            if sheep.x < -20 {
                continue
            }

            sheep.stretch = !sheep.stretch
            newSheepFlock.append(sheep)
        }
        
        if newSheepFlock.count == 0 {
            newSheepFlock.append(sheep())
        }

        sheepFlock = newSheepFlock
    }
    
    // MARK: Managing sheep
    
    private var sheepFlock = [Sheep]()
    
    private var count = 0
    
    private func sheep() -> Sheep {
        let sheep = Sheep()
        sheep.x = 100
        sheep.y = Int(arc4random() % 50 + 5)
        sheep.jumpX = jumpX(sheep.y)
        
        return sheep
    }
    
    private func jumpX(_ y: Int) -> Int {
        // y = 4 / 3 * (x - 40)
        return y * 3 / 4 + 40
    }
    
    // MARK: Drawing view

    private let imageFence = NSImage(named: "fence")!
    
    private let imageSheep00 = NSImage(named: "sheep00")!
    
    private let imageSheep01 = NSImage(named: "sheep01")!
    
    private let colorGround = NSColor(deviceRed: 0.471, green: 1, blue: 0.471, alpha: 1)
    
    private let colorSky = NSColor(deviceRed: 0.5, green: 0.5, blue: 1, alpha: 1)
    
    private let pointCounter = NSPoint(x: 5, y: 85)

    override func draw(_ dirtyRect: NSRect) {
        
        drawBackground(dirtyRect)
        
        for sheep in sheepFlock {
            draw(sheep: sheep)
        }
        
        let counter = NSAttributedString(string: "\(count) sheep")
        counter.draw(at: pointCounter)
        
    }

    private func draw(sheep: Sheep) {

        if (sheep.stretch) {
            imageSheep01.draw(at: NSPoint(x: sheep.x, y: sheep.y), from: NSZeroRect,
                operation: .sourceOver, fraction: 1)
        } else {
            imageSheep00.draw(at: NSPoint(x: sheep.x, y: sheep.y), from: NSZeroRect,
                operation: .sourceOver, fraction: 1)
        }

    }

    private func drawBackground(_ dirtyRect: NSRect) {

        colorGround.set()
        NSRectFill(NSRect(x: 0, y:0, width: dirtyRect.width, height: dirtyRect.height))

        colorSky.set()
        NSRectFill(NSRect(x: 0, y: dirtyRect.height - 30, width: 100, height: 30))

        imageFence.draw(at: NSPoint(x: 25, y: 0), from: NSZeroRect,
            operation: .sourceOver, fraction: 1)
        
    }

}
