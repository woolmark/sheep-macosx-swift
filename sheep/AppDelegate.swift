//
//  AppDelegate.swift
//  sheep
//
//  Created by Naoki Takimura on 6/3/14.
//  Copyright (c) 2014 Naoki Takimura. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var window: NSWindow!

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
}
